package store

import akka.http.scaladsl.model.Uri
import store.UriExtension.UriExtensionParseException

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.concurrent.Promise
import scala.util.Try

trait UriExtension {
  implicit class UriHelper(uri: Uri) {
    lazy val queryMap: Map[String, String] = uri.query().toMap

    def queryInt(keys: String*): Try[Map[String, Int]] = queryInt(keys.toSet)

    def queryInt(keys: Set[String]): Try[Map[String, Int]] =
      Try({

        val missingKeys = keys.diff(queryMap.keySet)
        if (missingKeys.nonEmpty) throw UriExtensionParseException(s"missing keys: ${missingKeys.mkString(", ")}")

        val converted: Map[String, Try[Int]] = queryMap
          .filterKeys(keys)
          .map { case (key, value) => key -> Try(value.toInt) }

        val (failed, successful) = converted.partition { case (_, value) => value.isFailure }
        if (failed.nonEmpty) throw UriExtensionParseException(s"key(s): ${failed.keys.mkString(", ")} must be 'int'")

        successful.map { case (key, value) => key -> value.get }
      })

    def queryBoolean(keys: String*): Try[Map[String, Boolean]] = queryBoolean(keys.toSet)

    def queryBoolean(keys: Set[String]): Try[Map[String, Boolean]] =
      Try({
        val missingKeys = keys.diff(queryMap.keySet)
        if (missingKeys.nonEmpty) throw UriExtensionParseException(s"missing keys: ${missingKeys.mkString(", ")}")

        val converted: Map[String, Try[Boolean]] = queryMap
          .filterKeys(keys)
          .map { case (key, value) => key -> Try(value.toBoolean) }

        val (failed, successful) = converted.partition { case (_, value) => value.isFailure }
        if (failed.nonEmpty)
          throw UriExtensionParseException(s"key(s): ${failed.keys.mkString(", ")} must be 'boolean'")

        successful.map { case (key, value) => key -> value.get }
      })

    def queryIntFuture(keys: String*)(implicit executionContext: ExecutionContext): Future[Map[String, Int]] =
      queryIntFuture(keys.toSet)

    def queryIntFuture(keys: Set[String])(implicit executionContext: ExecutionContext): Future[Map[String, Int]] = {
      val p = Promise[Map[String, Int]]
      Future { p.complete(queryInt(keys)) }
      p.future
    }

    def queryBooleanFuture(keys: String*)(implicit executionContext: ExecutionContext): Future[Map[String, Boolean]] =
      queryBooleanFuture(keys.toSet)

    def queryBooleanFuture(
      keys: Set[String]
    )(implicit executionContext: ExecutionContext
    ): Future[Map[String, Boolean]] = {
      val p = Promise[Map[String, Boolean]]
      Future { p.complete(queryBoolean(keys)) }
      p.future
    }

  }
}

object UriExtension {
  case class UriExtensionParseException(text: String) extends Exception(text)
}
