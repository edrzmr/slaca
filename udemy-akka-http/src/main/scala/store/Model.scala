package store

object Model {
  final case class Guitar(make: String, model: String)
}
