package store

import akka.actor.ActorRef
import akka.http.scaladsl.model.HttpMethods.GET
import akka.http.scaladsl.model.HttpMethods.POST
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.Uri
import akka.pattern.ask
import akka.stream.Materializer
import akka.util.Timeout
import spray.json._
import store.GuitarDBActor.Message._
import store.GuitarDBActor.Row
import store.Model.Guitar
import store.Router.RequestHandler
import store.UriExtension.UriExtensionParseException

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps

object Router {

  type RequestHandler = HttpRequest => Future[HttpResponse]

  def apply(executionContext: ExecutionContext, materializer: Materializer, dbActor: ActorRef): Router =
    new Router(executionContext, materializer, dbActor)
}

class Router(executionContext: ExecutionContext, materializer: Materializer, val dbActor: ActorRef)
    extends JsonFormat
    with UriExtension {

  private val futureTimeout                 = 3 seconds
  implicit private val ec: ExecutionContext = executionContext
  implicit private val mat: Materializer    = materializer
  implicit private val askTimeout: Timeout  = Timeout(futureTimeout)

  val handler: RequestHandler = {
    case HttpRequest(GET, Uri.Path("/"), _, _, _) => Future { Response("welcome to akka upside down!").asOk }

    case HttpRequest(GET, Uri.Path("/api/v1/guitars"), _, _, _) =>
      (dbActor ? FindAllGuitars)
        .mapTo[List[Guitar]]
        .map { guitars =>
          Response(guitars.toJson).asOk
        }
        .recover {
          case e =>
            e.printStackTrace()
            HttpResponse(StatusCodes.InternalServerError, entity = e.toString)
        }

    case HttpRequest(GET, uri @ Uri.Path("/api/v1/guitar"), _, _, _) =>
      uri
        .queryIntFuture("id")
        .flatMap { queryMap =>
          (dbActor ? FindGuitar(queryMap("id")))
            .mapTo[Option[Guitar]]
            .map {
              case None         => Response(s"guitar with 'id': ${queryMap("id")} not found").asNotFound
              case Some(guitar) => Response(guitar.toJson).asOk
            }
            .recover {
              case e: UriExtensionParseException => Response(e).asBadRequest
              case e =>
                e.printStackTrace()
                HttpResponse(StatusCodes.InternalServerError, entity = e.toString)
            }
        }

    case HttpRequest(POST, uri @ Uri.Path("/api/v1/guitar/inventory"), _, _, _) =>
      uri
        .queryIntFuture("id", "quantity")
        .flatMap { queryMap =>
          (dbActor ? AddQuantity(queryMap("id"), queryMap("quantity")))
            .mapTo[Option[Row]]
            .map {
              case None        => Response(s"guitar with id: ${queryMap("id")}").asNotFound
              case Some(stock) => Response(s"id: ${queryMap("id")}, stock: $stock").asOk
            }
        }
        .recover {
          case e: UriExtensionParseException => Response(e).asBadRequest
          case e =>
            e.printStackTrace()
            HttpResponse(StatusCodes.InternalServerError, entity = e.toString)
        }

    case HttpRequest(GET, uri @ Uri.Path("/api/v1/guitar/inventory"), _, _, _) =>
      uri
        .queryBooleanFuture("inStock")
        .flatMap { queryMap =>
          (if (queryMap("inStock")) (dbActor ? FindInStock).mapTo[List[Guitar]]
           else (dbActor ? FindOutOfStock).mapTo[List[Guitar]])
            .map { guitars =>
              Response(guitars.toJson).asOk
            }
        }
        .recover {
          case e: UriExtensionParseException => Response(e).asBadRequest
          case e =>
            e.printStackTrace()
            HttpResponse(StatusCodes.InternalServerError, entity = e.toString)
        }

    case HttpRequest(POST, Uri.Path("/api/v1/guitar"), _, entity, _) =>
      val futureGuitar: Future[Guitar] = entity
        .toStrict(futureTimeout)
        .map { _.data.utf8String.parseJson.convertTo[Guitar] }

      futureGuitar
        .flatMap { guitar =>
          println(s"guitar to create: $guitar")
          (dbActor ? CreateGuitar(guitar))
            .mapTo[GuitarCreated]
            .map {
              case GuitarCreated(id) => HttpResponse(StatusCodes.Created, entity = s"'id': $id")
            }
        }
        .recover {
          case e: spray.json.JsonParser.ParsingException => Response(e).asBadRequest
          case e =>
            e.printStackTrace()
            HttpResponse(StatusCodes.InternalServerError, entity = e.toString)
        }

    case request: HttpRequest =>
      request.discardEntityBytes()
      Future { Response(s"Resource not found: ${request.uri.path}").asNotFound }
  }
}
