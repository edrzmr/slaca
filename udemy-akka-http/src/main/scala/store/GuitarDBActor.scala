package store

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.Props
import store.Model.Guitar

object GuitarDBActor {

  val props: Props = Props[GuitarDBActor]()

  final case class Row(guitar: Guitar, stock: Int = 0)

  object Message {
    sealed trait Message extends Product with Serializable

    final case object FindAllGuitars extends Message

    final case class FindGuitar(id: Int) extends Message
    final case object FindInStock        extends Message
    final case object FindOutOfStock     extends Message

    final case class CreateGuitar(guitar: Guitar)        extends Message
    final case class AddQuantity(id: Int, quantity: Int) extends Message

    final case class GuitarCreated(id: Int) extends Message
  }
}

class GuitarDBActor extends Actor with ActorLogging {

  import GuitarDBActor._

  var db: Map[Int, Row] = Map.empty
  var currentId: Int    = 0

  override def receive: Receive = {
    case Message.CreateGuitar(guitar) =>
      db = db + (currentId -> Row(guitar))
      sender() ! Message.GuitarCreated(currentId)
      currentId += 1

    case Message.FindAllGuitars =>
      sender() ! db.values.toList.map { _.guitar }
    case Message.FindGuitar(id) => sender() ! db.get(id).map { _.guitar }

    case Message.FindInStock =>
      val inStockList: List[Guitar] = db.foldLeft(List.empty[Guitar]) {
        case (acc, (_, row)) if row.stock > 0 => row.guitar :: acc
        case (acc, _)                         => acc
      }
      sender() ! inStockList.reverse

    case Message.FindOutOfStock =>
      val outOfStockList: List[Guitar] = db.foldLeft(List.empty[Guitar]) {
        case (acc, (_, row)) if row.stock <= 0 => row.guitar :: acc
        case (acc, _)                          => acc
      }

      sender() ! outOfStockList.reverse

    case Message.AddQuantity(id, quantity) =>
      val maybeRow: Option[Row] = db.get(id)
      val maybeNewRow: Option[Row] = maybeRow.map { row =>
        row.copy(stock = row.stock + quantity)
      }
      maybeNewRow.foreach { newRow =>
        db = db + (id -> newRow)
      }

      sender() ! maybeNewRow
  }

}
