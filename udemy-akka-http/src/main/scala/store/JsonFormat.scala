package store

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol
import store.Model.Guitar

trait JsonFormat extends SprayJsonSupport with DefaultJsonProtocol {

  import spray.json._

  implicit val guitarFormat: RootJsonFormat[Guitar] = jsonFormat2(Guitar)
}
