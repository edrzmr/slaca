package store

import akka.actor.ActorSystem
import akka.actor.CoordinatedShutdown
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.IncomingConnection
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import akka.util.Timeout
import akka.Done
import store.GuitarDBActor.Message._
import store.Model.Guitar
import sun.misc.Signal

import scala.concurrent.Await
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Failure
import scala.util.Success

object HttpServer extends App with JsonFormat with UriExtension {

  implicit val system: ActorSystem                = ActorSystem("Store")
  implicit val materializer: ActorMaterializer    = ActorMaterializer()
  implicit val executionContext: ExecutionContext = system.dispatcher
  implicit val askTimeout: Timeout                = Timeout(3 seconds)

  val guitarDBActor = system.actorOf(GuitarDBActor.props, "GuitarDBActor")
  val initialDatabase: List[Guitar] =
    List(
      Guitar("A", "A model"),
      Guitar("B", "B model"),
      Guitar("C", "C model")
    )
  initialDatabase.foreach { guitar =>
    (guitarDBActor ? CreateGuitar(guitar)).mapTo[GuitarCreated].foreach { guitarId =>
      println(s"guitar created: $guitar with id: $guitarId")
    }
  }

  val router = Router(executionContext, materializer, guitarDBActor)

  val connectionSink = Sink.foreach[IncomingConnection] { connection =>
    val host = connection.remoteAddress.getHostString
    val port = connection.remoteAddress.getPort
    println(s"Accepted incoming connection from: $host:$port")
    connection.handleWithAsyncHandler(router.handler)
  }

  val serverSource = Http().bind("localhost", 8388)

  val bindingFuture = serverSource.to(connectionSink).run()

  bindingFuture.onComplete {
    case Failure(exception) => throw exception
    case Success(binding) =>
      val host = binding.localAddress.getHostString
      val port = binding.localAddress.getPort
      println(s"Server binding successful on: http://$host:$port")
  }

  def gracefulShutdown(signal: Signal): Unit = {
    println(s"\n>> signal received: $signal, going to shutdown...")

    bindingFuture.foreach { binding =>
      val shutdown = CoordinatedShutdown(system)

      shutdown.addTask(CoordinatedShutdown.PhaseServiceUnbind, "http-unbind") { () =>
        println(">> http unbind...")
        binding.unbind().map(_ => Done)
      }

      shutdown.addTask(CoordinatedShutdown.PhaseServiceRequestsDone, "http-graceful-terminate") { () =>
        println(">> http terminate...")
        binding.terminate(10.seconds).map(_ => Done)
      }

      shutdown.addTask(CoordinatedShutdown.PhaseServiceStop, "http-shutdown") { () =>
        println(">> http shutdown...")
        Http().shutdownAllConnectionPools().map(_ => Done)
      }

      shutdown.run(CoordinatedShutdown.JvmExitReason)
    }

    val terminateFuture = bindingFuture
      .flatMap { _ =>
        system.terminate()
      }

    terminateFuture.onComplete {
      case Failure(exception) =>
        println(s">> something weird on shutdown: ${exception.getMessage}")
        exception.printStackTrace()
      case Success(_) =>
        println(">> bye bye ;)")
    }

    Await.result(terminateFuture, 10 seconds)
  }

  Signal.handle(new Signal("TERM"), (signal: Signal) => gracefulShutdown(signal))
  Signal.handle(new Signal("INT"), (signal: Signal) => gracefulShutdown(signal))

}
