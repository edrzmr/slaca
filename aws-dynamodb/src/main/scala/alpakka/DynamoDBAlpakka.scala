package alpakka

import java.util

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.dynamodb.scaladsl.DynamoDb
import akka.stream.scaladsl.{Keep, Sink, Source}
import com.amazonaws.services.dynamodbv2.model.{
  AttributeValue,
  ScanRequest,
  ScanResult
}

import scala.collection.immutable
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration
import scala.language.postfixOps

object DynamoDBAlpakka extends App {

  implicit val system: ActorSystem = ActorSystem("akka-dynamodb")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContext = system.dispatcher

  val tableName = "confserver.hmg.platform"
  println(tableName)

  val scanRequest = new ScanRequest().withTableName(tableName)

  val source: Source[ScanResult, NotUsed] =
    DynamoDb.source(new ScanRequest().withTableName(tableName))

  val sink = Sink.seq[ScanResult] //(x => println(s"--- $x"))

  val futureScanResults: Future[immutable.Seq[ScanResult]] =
    source.toMat(sink)(Keep.right).run()

  import scala.collection.JavaConverters._

  futureScanResults.foreach { scanResults =>
    scanResults.foreach { scanResult =>
      val list = scanResult.getItems.asScala.toList

      list.foreach { item: util.Map[String, AttributeValue] =>
        println(s"----")

        val i = item.asScala.toMap
        i.foreach { case (k, v) =>
          println(s"$k: $v")
        }
      }

    }
  }

  //futureScanResults.flatMap(_ => system.terminate())

  Await.result(system.whenTerminated, Duration.Inf)
  println("depois")

}
