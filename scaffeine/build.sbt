name := "scaffeine"

version := "0.1"

scalaVersion := "2.13.0"

libraryDependencies ++= Seq(
  "com.github.blemale" %% "scaffeine" % "3.1.0" % Compile,
  "org.scalatest" %% "scalatest" % "3.0.8" % Test
)

