package playground

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow
import akka.stream.scaladsl.Keep
import akka.stream.scaladsl.RunnableGraph
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Source
import akka.NotUsed

import scala.concurrent.Future
import scala.util.Failure
import scala.util.Success

object Second extends App {

  val actorSystem: ActorSystem = ActorSystem("SecondPlayground")

  implicit val system: ActorSystem             = actorSystem
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  import system.dispatcher

  val sourceOfIntegers = Source(1 to 10)
  val sinkLastInt      = Sink.last[Int]
  val graph            = sourceOfIntegers.toMat(sinkLastInt)(Keep.right)
  graph.run().onComplete {
    case Failure(exception) => println(s"something wrong: $exception")
    case Success(last)      => println(s"last element: $last")
  }

  val sourceOfSentences = Source(
    List(
      "pinto que pia de baixo da pia",
      "vida loka cabulosa",
      "pra cair o c* da bund*",
      "dedo no c* e gritaria"
    ))

  val sinkCounter: Sink[String, Future[Int]] = Sink.fold[Int, String](0) {
    case (acc, s) => acc + s.split(' ').length
  }
  val g1 = sourceOfSentences.toMat(sinkCounter)(Keep.right).run()
  g1.foreach(count => println(s"[g1] number of words: $count"))

  val g2 = sourceOfSentences.runWith(sinkCounter)
  g2.foreach(count => println(s"[g2] number of words: $count"))

  val g3 = sourceOfSentences.runFold(0)((acc, words) => acc + words.split(' ').length)
  g3.foreach(count => println(s"[g3] number of words: $count"))

  val flow: Flow[String, Int, NotUsed] = Flow[String].map(s => s.split(' ').length)
  val flowReducer                      = Flow[Int].reduce(_ + _)
  val sink                             = Sink.reduce[Int](_ + _)

  val x: RunnableGraph[Future[Int]] = sourceOfSentences
    .viaMat(flow)(Keep.right)
    .viaMat(flowReducer)(Keep.right)
    .toMat(Sink.head)(Keep.right)

  x.run().foreach(println)

  actorSystem.terminate()
}
