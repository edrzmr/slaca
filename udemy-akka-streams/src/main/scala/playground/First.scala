package playground

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, RunnableGraph, Sink, Source}
import akka.NotUsed

object First extends App {

  implicit val actorSystem: ActorSystem        = ActorSystem("Playground")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val words = List(
    "eder",
    "aaaaaaaaaaaaaaaaa",
    "bbbbbbbbbbbbbbbbb",
    "bbb",
  )

  val source: Source[String, NotUsed] = Source(words)
  val flowFilter = Flow[String].filter(s => s.length < 5)
  val flowTake = Flow[String].take(2)
  val graph: RunnableGraph[NotUsed] = source.via(flowFilter).via(flowTake).to(Sink.foreach(println))
  graph.run()

  Source(words).filter(_.length < 5).take(2).runForeach(println)

  actorSystem.terminate()
}
