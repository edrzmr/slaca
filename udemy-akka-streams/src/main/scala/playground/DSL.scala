package playground

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.ClosedShape
import akka.stream.scaladsl.RunnableGraph
import akka.stream.scaladsl.Broadcast
import akka.stream.scaladsl.GraphDSL
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Source
import akka.NotUsed

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object DSL extends App {

  implicit val system: ActorSystem             = ActorSystem("DSL")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val input = Source(1 to 5)

  val outputA = Sink.foreach[Int] { a =>
    println(s"[a] data: $a")
  }

  val outputB = Sink.foreach[Int] { b =>
    println(s"[b] data: $b")
  }

  val graph: RunnableGraph[NotUsed] = RunnableGraph.fromGraph(
    GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._

      val broadcast = builder.add(Broadcast[Int](2))

      input ~> broadcast

      broadcast.out(0) ~> outputA
      broadcast.out(1) ~> outputB

      ClosedShape
    }
  )
  graph.run()


  system.terminate
  Await.result(system.whenTerminated, Duration.Inf)
}
