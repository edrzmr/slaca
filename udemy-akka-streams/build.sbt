name := "udemy-akka-streams"

version := "0.0.1-SNAPSHOT"

scalaVersion := "2.12.8"

val akkaVersion      = "2.5.20"
val scalaTestVersion = "3.0.5"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream"         % akkaVersion,
  "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit"        % akkaVersion,
  "org.scalatest"     %% "scalatest"           % scalaTestVersion % Test
)
